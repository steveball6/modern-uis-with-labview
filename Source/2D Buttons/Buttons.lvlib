﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">4337814</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)_!!!*Q(C=\&gt;5R4A*"&amp;-&lt;R4W.B3W^BO-+\!F?A.L&amp;YF&lt;417H+&amp;6^FT!"OO]+Z!O!'*M&lt;!Q"P]T0I(%2"IVRLDL,0$.TLS@M]MC^?V=/N0WW,&lt;Y&gt;/P^5GN;V,%&gt;WMOOP\V`#`:`/OQ`C08_5@N_,29@RB`/XQPO[X`I`^2`:.0`^L`^\?XW[*@AW\](*WI0)FL3AO9U5SO\4@)E4`)E4`)E$`)A$`)A$`)A&gt;X)H&gt;X)H&gt;X)H.X)D.X)D.X)D\TOZS%5O=EB*];21-GES18)S&amp;#7(R*.Y%E`CY;-34_**0)EH]8#+%E`C34S**`%Q4)EH]33?R*.YG+J,MO`E?")0USPQ"*\!%XA#$S56?!*!5#S9/*A%BI,/Y%XA#4S"B\=+0)%H]!3?Q%/X!E`A#4S"*`!QJ+^+&gt;%X&lt;S@%QD2S0YX%]DM@R-,5=D_.R0)\(]6"/DM@R/!CHI$-Z"$G$H"/=$Y\(]@!CR_.Y()`D=4RU^3PE@77;JOXE?!S0Y4%]BM@Q-)5-D_%R0)&lt;(]$#N$)`B-4S'R`"13I&lt;(]"A?!W)5J&lt;S-S9S"REF'9(DY[X?,^;M587*^F_LG6&gt;W5KJN.&gt;2/J&lt;A\626&gt;&gt;4.6&amp;5CW_;F&amp;6C[6;".5`JU+L-+ICKM(N2'UYLGB,P@V3TGETWJAWIAVJAT&lt;UCU`=&lt;$:;L6:;,J@^&gt;XE_HWMWGWE](GMU'GEY('IQ'/Q?!Z@MOQ@#`LHU?('^@JH=8TX@4/_?*P@LBYPJ_I&gt;_HX`"MV'H/KT"'LU#WD[!NA!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Button.lvclass" Type="LVClass" URL="../../Buttons/Button.lvclass"/>
	<Item Name="Icon and Text Layout.vi" Type="VI" URL="../../Layout/Icon and Text Layout.vi"/>
	<Item Name="ButtonType.ctl" Type="VI" URL="../../Buttons/ButtonType.ctl"/>
</Library>
